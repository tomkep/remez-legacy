CPPFLAGS = -6 -O1 -W -v
OBJECTS   = remezw.obj remez.obj remez1.obj fobjects.obj eq.obj
RESOURCES = remez.res

remez.exe:  $(OBJECTS) $(RESOURCES)
	ilink32 /aa/Tpe/x c0w32.obj $(OBJECTS), $@, , import32 cw32, , $(RESOURCES)

clean:
	del *.obj
	del remez.tds
	del remez.il?
	del remez.res
