#include <math.h>

static long double const delta = 1E-9L;

#ifndef __GNUC__
static inline long double fabsl(long double x)
{
  return (x >= 0 ? x : -x);
}
#endif

int Eq(long double x1, long double x2)
{
  if(x1 == 0)
    return (fabsl(x2) < delta);
  if(x2 == 0)
    return (fabsl(x1) < delta);
  return (delta * fabsl(x1 + x2) > fabsl(x1 - x2));
}
