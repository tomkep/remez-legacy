CXXFLAGS += -Wall -Wextra -pedantic -O2
CC = g++

remez:  remezd.o remez.o remez1.o fobjects.o eq.o
	g++ -o $@ $^

clean:
	rm *.o
