OBJECTS = remezd.obj remez.obj remez1.obj fobjects.obj eq.obj
MODEL = s

.cpp.obj:
	bcc -m$(MODEL) -2 -f87 -w -c {$< }

remez.exe: $(OBJECTS)
	tlink /c/Tde/x c0$(MODEL).obj $(OBJECTS), $<,, graphics fp87 math$(MODEL) c$(MODEL)

clean:
	del *.obj
