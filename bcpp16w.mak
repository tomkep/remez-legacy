OBJECTS   = remezw.obj remez.obj remez1.obj fobjects.obj eq.obj
MODEL     = s

.cpp.obj:
	bcc -m$(MODEL) -2 -W -w -c {$< }

remez.exe:  $(OBJECTS) $(RESOURCES)
	tlink /c/Twe/x c0w$(MODEL).obj $(OBJECTS), $@,, import cw$(MODEL) mathw$(MODEL), remez
	rc /k /iy:\bcpp31\include remez.rc $@

clean:
	del *.obj
	del remez.res
