#ifndef __REMEZ_H
#define __REMEZ_H

#ifndef __FOBJECTS_H
#include "fobjects.h"
#endif

extern Float Delta;
extern Float Q;
extern Float RequiredQ;
extern unsigned int S;

void InitRemez(const Filter &AFIR, int Alen, FilterTypes AT);
int RemezIteration(SearchTypes s);
int Remez(SearchTypes s, int maxiter);
Vector CalcH(void);

int DesignMultiBandFIR(const Filter F, Float d, FilterTypes ft, SearchTypes st, Vector &H);
int DesignDifferentiator(Float UpLimit, Float ER, Float d, SearchTypes st, Vector &H);

#endif
